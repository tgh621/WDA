package com.farm.wda.adaptor.impl;

import java.io.File;
import com.farm.wda.adaptor.DocConvertorBase;
import com.farm.wda.util.FileUtil;

public class NoneConvertor extends DocConvertorBase {

	public void run(File file, String fileTypeName, File targetFile) {
		targetFile.getParentFile().mkdirs();
		FileUtil.wirteInfo(targetFile, file.getName());
		return;
	}
}
